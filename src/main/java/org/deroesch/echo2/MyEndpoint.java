package org.deroesch.echo2;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyEndpoint {

	private static final Logger logger = LogManager.getLogger(MyEndpoint.class);

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to the
	 * client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String sample() {

		JsonObject msg = new JsonObject();
		msg.addProperty("Message", "Here's a sample JSON element");
		msg.addProperty("Created at", new Date().toString());

		String s = msg.toString();
		logger.info(s);
		return s;
	}

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to the
	 * client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Path("old")
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		String s = "Got it!";
		logger.info(s);
		return s;
	}

	/**
	 * Accepts and prints a JSON string from an outside caller
	 * 
	 * @param s The string to accept
	 */
	@POST
	@Path("inbound")
	@Consumes(MediaType.APPLICATION_JSON)
	public String echo(String s) {
		logger.info(s);
		return s;
	}
}
