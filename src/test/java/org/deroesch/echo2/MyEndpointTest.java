package org.deroesch.echo2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.deroesch.echo2.MyEndpoint;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MyEndpointTest extends MyEndpoint {

	private static final MyEndpoint ENDPOINT = new MyEndpoint();
	private static final String TEST_STRING = "{\"Example\":\"Simple message.\"}";

	private static final Logger logger = LogManager.getLogger(MyEndpoint.class);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		logger.info("");
		logger.info("*** Starting Tests ***");
		logger.info("");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		logger.info("");
		logger.info("*** Testing Completed ***");
		logger.info("");
	}

	@Test
	final void testSample() {
		assert(ENDPOINT.sample().length() > 0);
	}
	@Test
	final void getGetIt() {
		assertEquals("Got it!", ENDPOINT.getIt());
	}

	@Test
	final void testEcho() {
		assertEquals(TEST_STRING, ENDPOINT.echo(TEST_STRING));
	}

}
